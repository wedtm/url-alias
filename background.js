'use strict';

import util from './util.js'

// event listeners for web-navigation
var filter = {
    url : [{}]
};

chrome.webNavigation.onErrorOccurred.addListener((details)=>{
    var alias = details.url.split("///")[1];

    if(!alias){
        return;
    }

    util.db.get(alias).then(result=>{
        chrome.tabs.update(details.tabId, {url: result[alias]});
    }).catch(e=>{
        //if alias does not exist -> redirect to URL-Alias website
        chrome.tabs.update(details.tabId, {url: "url-alias.html?new="+alias});
    }); 
}, filter);

chrome.webNavigation.onCompleted.addListener(async function(details){
    chrome.pageAction.show(details.tabId);

    var tab = await util.getCurrentTab();
    util.db.find(tab.url).then(result=>{
        util.icon.setSelected();
    });

}, filter);

// event listener for add/remove operation
chrome.runtime.onMessage.addListener((message, sender, response)=>{
    
    if(message.action == 'add'){
        util.icon.setSelected();
    }else if(message.action == 'remove'){
        util.icon.set();
    }
});

// shows icon for all pages
// chrome.tabs.query({}).then((tabs)=>{
//     for (var tab in tabs){
//         chrome.action.show(tab.id);
//     }
// });